# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :dnd_spellbook,
  ecto_repos: [DndSpellbook.Repo]

# Configures the endpoint
config :dnd_spellbook, DndSpellbook.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ABpMzM/twUh15b28yI/9eGuZZHga++VcQFh29Jk44ZwHsz2k9ZjPdCqb7lJOlT3+",
  render_errors: [view: DndSpellbook.ErrorView, accepts: ~w(html json)],
  pubsub: [name: DndSpellbook.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
