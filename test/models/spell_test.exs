defmodule DndSpellbook.SpellTest do
  use DndSpellbook.ModelCase

  alias DndSpellbook.Spell

  @valid_attrs %{
    area_of_effect: "some content",
    casting_time: "some content",
    components: "some content",
    duration: "some content",
    effect: "some content",
    name: "some content",
    range: "some content",
    saving_throw: "some content",
    schools: "some content",
    level: 1,
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Spell.changeset(%Spell{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Spell.changeset(%Spell{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "changeset has a level" do
    changeset = Spell.changeset(%Spell{}, @valid_attrs)
    assert Ecto.Changeset.get_field(changeset, :level)
  end

  test "level is mandatory" do
    changeset = Spell.changeset(%Spell{}, %{@valid_attrs | level: nil})
    refute changeset.valid?
  end

  test "saving throw is not mandatory" do
    changeset = Spell.changeset(%Spell{}, %{@valid_attrs | saving_throw: nil})
    assert changeset.valid?

    changeset = Spell.changeset(%Spell{}, %{@valid_attrs | saving_throw: ""})
    assert changeset.valid?
  end
end
