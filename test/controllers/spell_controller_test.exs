defmodule DndSpellbook.SpellControllerTest do
  use DndSpellbook.ConnCase

  alias DndSpellbook.Spell
  @valid_attrs %{area_of_effect: "some content", casting_time: "some content", components: "some content", duration: "some content", effect: "some content", name: "some content", range: "some content", saving_throw: "some content", schools: "some content", level: 1}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, spell_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing spells"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, spell_path(conn, :new)
    assert html_response(conn, 200) =~ "New spell"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, spell_path(conn, :create), spell: @valid_attrs
    assert redirected_to(conn) == spell_path(conn, :index)
    assert Repo.get_by(Spell, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, spell_path(conn, :create), spell: @invalid_attrs
    assert html_response(conn, 200) =~ "New spell"
  end

  test "shows chosen resource", %{conn: conn} do
    spell = Repo.insert! %Spell{}
    conn = get conn, spell_path(conn, :show, spell)
    assert html_response(conn, 200) =~ "Show spell"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, spell_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    spell = Repo.insert! %Spell{}
    conn = get conn, spell_path(conn, :edit, spell)
    assert html_response(conn, 200) =~ "Edit spell"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    spell = Repo.insert! %Spell{}
    conn = put conn, spell_path(conn, :update, spell), spell: @valid_attrs
    assert redirected_to(conn) == spell_path(conn, :show, spell)
    assert Repo.get_by(Spell, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    spell = Repo.insert! %Spell{}
    conn = put conn, spell_path(conn, :update, spell), spell: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit spell"
  end

  test "deletes chosen resource", %{conn: conn} do
    spell = Repo.insert! %Spell{}
    conn = delete conn, spell_path(conn, :delete, spell)
    assert redirected_to(conn) == spell_path(conn, :index)
    refute Repo.get(Spell, spell.id)
  end
end
