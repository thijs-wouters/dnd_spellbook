defmodule DndSpellbook.SpellController do
  use DndSpellbook.Web, :controller

  alias DndSpellbook.Spell

  def index(conn, _params) do
    spells = Repo.all(Spell)
    render(conn, "index.html", spells: spells)
  end

  def new(conn, _params) do
    changeset = Spell.changeset(%Spell{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"spell" => spell_params}) do
    changeset = Spell.changeset(%Spell{}, spell_params)

    case Repo.insert(changeset) do
      {:ok, _spell} ->
        conn
        |> put_flash(:info, "Spell created successfully.")
        |> redirect(to: spell_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    spell = Repo.get!(Spell, id)
    render(conn, "show.html", spell: spell)
  end

  def edit(conn, %{"id" => id}) do
    spell = Repo.get!(Spell, id)
    changeset = Spell.changeset(spell)
    render(conn, "edit.html", spell: spell, changeset: changeset)
  end

  def update(conn, %{"id" => id, "spell" => spell_params}) do
    spell = Repo.get!(Spell, id)
    changeset = Spell.changeset(spell, spell_params)

    case Repo.update(changeset) do
      {:ok, spell} ->
        conn
        |> put_flash(:info, "Spell updated successfully.")
        |> redirect(to: spell_path(conn, :show, spell))
      {:error, changeset} ->
        render(conn, "edit.html", spell: spell, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    spell = Repo.get!(Spell, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(spell)

    conn
    |> put_flash(:info, "Spell deleted successfully.")
    |> redirect(to: spell_path(conn, :index))
  end
end
