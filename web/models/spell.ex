defmodule DndSpellbook.Spell do
  use DndSpellbook.Web, :model

  schema "spells" do
    field :name, :string
    field :level, :integer
    field :schools, :string
    field :range, :string
    field :duration, :string
    field :area_of_effect, :string
    field :components, :string
    field :casting_time, :string
    field :saving_throw, :string
    field :effect, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :schools, :range, :duration, :area_of_effect, :components, :casting_time, :saving_throw, :effect, :level])
    |> validate_required([:name, :schools, :range, :duration, :area_of_effect, :components, :casting_time, :effect, :level])
  end
end
