defmodule DndSpellbook.Repo.Migrations.AddLevelToSpell do
  use Ecto.Migration

  def change do
    alter table(:spells) do
      add :level, :integer, default: 0
    end
  end
end
