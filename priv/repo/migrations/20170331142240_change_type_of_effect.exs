defmodule DndSpellbook.Repo.Migrations.ChangeTypeOfEffect do
  use Ecto.Migration

  def change do
    alter table(:spells) do
      modify :effect, :text
    end
  end
end
