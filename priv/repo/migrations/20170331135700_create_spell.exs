defmodule DndSpellbook.Repo.Migrations.CreateSpell do
  use Ecto.Migration

  def change do
    create table(:spells) do
      add :name, :string
      add :schools, :string
      add :range, :string
      add :duration, :string
      add :area_of_effect, :string
      add :components, :string
      add :casting_time, :string
      add :saving_throw, :string
      add :effect, :string

      timestamps()
    end

  end
end
